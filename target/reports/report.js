$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("login.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#language: pt"
    }
  ],
  "line": 4,
  "name": "Login no aplicativo",
  "description": "Como um usuário registrado\nEu quero fazer login no aplicativo\nPara acessar meus recursos pessoais",
  "id": "login-no-aplicativo",
  "keyword": "Funcionalidade",
  "tags": [
    {
      "line": 3,
      "name": "@login"
    }
  ]
});
formatter.before({
  "duration": 9378036667,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "Login com sucesso",
  "description": "",
  "id": "login-no-aplicativo;login-com-sucesso",
  "type": "scenario",
  "keyword": "Cenário",
  "tags": [
    {
      "line": 26,
      "name": "@realizarLogout"
    }
  ]
});
formatter.step({
  "line": 28,
  "name": "que estou na tela inicial do aplicativo Marvellopedia",
  "keyword": "Dado "
});
formatter.step({
  "line": 29,
  "name": "preencho os campos e-mail e senha na tela de login",
  "keyword": "E "
});
formatter.step({
  "line": 30,
  "name": "clicar no botão login",
  "keyword": "E "
});
formatter.step({
  "line": 31,
  "name": "acesso o menu perfil",
  "keyword": "E "
});
formatter.step({
  "line": 32,
  "name": "clico no botao Logout",
  "keyword": "Quando "
});
formatter.step({
  "line": 33,
  "name": "deslogo do aplicativo",
  "keyword": "Entao "
});
formatter.match({
  "location": "CadastroSteps.que_estou_na_tela_inicial_do_aplicativo_Marvellopedia()"
});
formatter.result({
  "duration": 1114467834,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.preencho_os_campos_e_mail_e_senha_na_tela_de_login()"
});
formatter.result({
  "duration": 868033750,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.clicar_no_botão_login()"
});
formatter.result({
  "duration": 1374966500,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.acesso_o_menu_perfil()"
});
formatter.result({
  "duration": 2947875667,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.clico_no_botao_Logout()"
});
formatter.result({
  "duration": 93170208,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.deslogo_do_aplicativo()"
});
formatter.result({
  "duration": 1966184625,
  "status": "passed"
});
});