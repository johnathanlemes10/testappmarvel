#language: pt

  @login
Funcionalidade: Login no aplicativo
  Como um usuário registrado
  Eu quero fazer login no aplicativo
  Para acessar meus recursos pessoais

  @loginSuccesso
  Cenário: Login com sucesso
    Dado que estou na tela inicial do aplicativo Marvellopedia
    E preencho os campos e-mail e senha na tela de login
    Quando clicar no botão login
    Entao o aplicativo apresenta a tela de quadrinhos

  @alterarSenha
  Cenário: Login com sucesso
    Dado que estou na tela inicial do aplicativo Marvellopedia
    E preencho os campos e-mail e senha na tela de login
    E clicar no botão login
    E acesso o menu perfil
    E acesso o menu senha
    Quando altero o campo senha
    Entao o aplicativo apresenta mensagem senha alterada com sucesso,

  @realizarLogout
  Cenário: Login com sucesso
    Dado que estou na tela inicial do aplicativo Marvellopedia
    E preencho os campos e-mail e senha na tela de login
    E clicar no botão login
    E acesso o menu perfil
    Quando clico no botao Logout
    Entao deslogo do aplicativo




