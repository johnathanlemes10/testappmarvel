#language: pt

 @heroi
Funcionalidade: Lista de Heróis
  Como um usuário do aplicativo Marvelopedia
  Eu quero visualizar uma lista de heróis
  Para que eu possa escolher um herói para ver mais informações

  Contexto: Login
    Dado que estou na tela inicial do aplicativo Marvellopedia
    E preencho os campos e-mail e senha na tela de login
    Quando clicar no botão login

  @listaHeroi
  Cenário: Lista de heróis é exibida corretamente
    Dado que estou na tela de lista de heróis
    Então devo ver uma lista de heróis com imagens e nomes
    E cada item da lista deve ter uma imagem, nome do heroi

  @BuscaHeroiPorNome
  Cenário: Busca por nome de herói
    Dado que estou na tela de lista de heróis
    E devo ver uma lista de heróis com imagens e nomes
    Quando digito Captain na barra de pesquisa
    Então devo ver uma lista de heróis que contém Captain no nome

  @BuscaHeroiPorNomeInexistente
  Cenário: Busca por nome de herói
    Dado que estou na tela de lista de heróis
    E devo ver uma lista de heróis com imagens e nomes
    Quando digito Teste na barra de pesquisa
    Então nao devo ver uma lista de heróis que contém Teste no nome







