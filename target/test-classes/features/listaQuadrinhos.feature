# language: pt

@listaQuadrinho
Funcionalidade: Tela de quadrinhos
Como um usuário registrado
Eu quero fazer login no aplicativo
Para acessar meus recursos pessoais

  Contexto: Login com sucesso
    Dado que estou na tela inicial do aplicativo Marvellopedia
    E preencho os campos e-mail e senha na tela de login
    E clicar no botão login


  @ListaCarregadaComSucesso
  Cenário: Exibir lista de quadrinhos
    Dado que estou na tela de quadrinhos
    Quando a lista de quadrinhos é exibida corretamente
    Entao cada quadrinho deve apresentar capa, título, e valor do quadrinho

  @pesquisaQuadrinho
  Cenário: Pesquisar quadrinhos
    Dado que estou na tela de quadrinhos
    Quando insiro um nome de quadrinho na barra de pesquisa
    Então a lista de quadrinhos é atualizada de acordo com o quadrinho pesquisado inserido

  @incluirQuadrinhoNoCarrinho
  Cenário: Selecionar quadrinho
    Dado que estou na tela de quadrinhos
    E insiro um nome de quadrinho na barra de pesquisa
    Quando seleciono um quadrinho na lista de quadrinhos
    Então a tela de detalhes do quadrinho é exibida com capa do quadrinho, título, descrição e valor
    E clico no botao adicionar

