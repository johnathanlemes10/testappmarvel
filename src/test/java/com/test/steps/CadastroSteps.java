package com.test.steps;

import com.test.screen.BaseScreen;
import com.test.screen.ScreenCadastro;
import com.test.screen.ScreenHome;
import com.test.screen.ScreenLogin;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.openqa.selenium.remote.RemoteWebElement;

public class CadastroSteps extends BaseScreen {
        ScreenCadastro cadastro = new ScreenCadastro(new RemoteWebElement());
        ScreenHome home = new ScreenHome();
        ScreenLogin login = new ScreenLogin();

    @Dado("^que estou na tela inicial do aplicativo Marvellopedia$")
    public void que_estou_na_tela_inicial_do_aplicativo_Marvellopedia()  {

        home.tituloAppMarvelopedia.isDisplayed();
    }

    @E("^clico no botão Criar uma conta$")
    public void clico_no_botão_Criar_uma_conta()  {
        cadastro.botaoCriarUmaConta.click();
    }

    @E("^o aplicativo redireciona para a tela de cadastro$")
    public void o_aplicativo_redireciona_para_a_tela_de_cadastro()  {
        cadastro.tituloCadastro.isDisplayed();
    }

    @E("^preencho os campos e-mail e senha$")
    public void preencho_os_campos_e_mail_e_senha() {
        login.writeEmail();
        login.writePass();
        login.writePass1();
    }

    @Quando("^clicar no botão cadastrar$")
    public void clicar_no_botão_cadastrar()  {
        login.btnCadastrar.click();
    }

    @Entao("^o aplicativo apresenta mensagem de usuario cadastrado com sucesso$")
    public void o_aplicativo_apresenta_mensagem_de_usuario_cadastrado_com_sucesso()  {
        cadastro.msgCadastroComSucesso.isDisplayed();
    }

    @Entao("^o aplicativo apresenta mensagem de erro informando que o endereço de e-mail já está em uso$")
    public void o_aplicativo_apresenta_mensagem_de_erro_informando_que_o_endereço_de_e_mail_já_está_em_uso()  {
        cadastro.msgEmailExistente.isDisplayed();
    }

    @Entao("^devo receber uma mensagem de erro informando que a senha é obrigatória$")
    public void devo_receber_uma_mensagem_de_erro_informando_que_a_senha_é_obrigatória()  {
        cadastro.msgCamposObrigatoriosNaoPreenchidos.isDisplayed();

    }

    @E("^devo permanecer na tela de cadastro$")
    public void devo_permanecer_na_tela_de_cadastro() {
        cadastro.tituloCadastro.isDisplayed();
    }

    @E("^preencho o campo email com email inválido$")
    public void preencho_o_campo_email_com_email_inválido()  {
        login.writeEmailInvalido();
        login.writePass();
        login.writePass1();
        login.btnCadastrar.click();
    }

    @Entao ("^o aplicativo apresenta mensagem de Email invalido$")
    public void o_aplicativo_apresenta_mensagem_de_Email_invalido()  {
        cadastro.msgEmailInvalido.isDisplayed();
    }
}
