package com.test.steps;

import com.test.screen.*;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class ListaQuadrinhosSteps extends BaseScreen {

        ScreenQuadrinho quadrinho = new ScreenQuadrinho();
        ScreenLogin login = new ScreenLogin();
        ScreenCarrinho carrinho = new ScreenCarrinho();
        Utils utils = new Utils();

        @Dado("^que estou na tela de quadrinhos$")
        public void que_estou_na_tela_de_quadrinhos(){
              quadrinho.tituloTelaQuadrinhos.isDisplayed();
        }

        @Quando("^a lista de quadrinhos é exibida corretamente$")
        public void a_lista_de_quadrinhos_é_exibida_corretamente() {
                quadrinho.listaQuadrinhos.isDisplayed();
        }

        @Entao("^cada quadrinho deve apresentar capa, título, e valor do quadrinho$")
        public void cada_quadrinho_deve_apresentar_capa_título_e_valor_do_quadrinho()  {
                quadrinho.listaQuadrinhos.isDisplayed();
                quadrinho.capaQuadrinhos.isDisplayed();
                quadrinho.valorQuadrinho.isDisplayed();
        }

        @Quando("^insiro um nome de quadrinho na barra de pesquisa$")
        public void insiro_um_nome_de_quadrinho_na_barra_de_pesquisa()  {
                quadrinho.escreverPesquisa();

        }

        @Entao("^a lista de quadrinhos é atualizada de acordo com o quadrinho pesquisado inserido$")
        public void a_lista_de_quadrinhos_é_atualizada_de_acordo_com_o_quadrinho_pesquisado_inserido()  {
                quadrinho.listaPesquisaQuadrinhos.isDisplayed();
                quadrinho.capaPesquisaQuadrinhos.isDisplayed();
                quadrinho.valorPesquisaQuadrinho.isDisplayed();
        }

        @Quando("^seleciono um quadrinho na lista de quadrinhos$")
        public void seleciono_um_quadrinho_na_lista_de_quadrinhos()  {
                quadrinho.capaPesquisaQuadrinhos.click();

        }

        @Entao("^a tela de detalhes do quadrinho é exibida com capa do quadrinho, título, descrição e valor$")
        public void a_tela_de_detalhes_do_quadrinho_é_exibida_com_capa_do_quadrinho_título_descrição_e_valor()  {
                quadrinho.capaPesquisaQuadrinhos.isDisplayed();
                utils.scroll(0.7, 0.2);
                quadrinho.valorPesquisaDescQuadrinho.isDisplayed();
        }

        @Entao("^clico no botao adicionar$")
        public void clico_no_botao_adicionar() {
                quadrinho.addQtdQuadrinho.click();
                quadrinho.addQuadrinhoAoCarrinho.click();
                carrinho.tituloTelaCarrinho.isDisplayed();
                carrinho.valotTotalCarrinho.isDisplayed();
        }

}
