package com.test.steps;

import com.test.screen.ScreenLogin;
import com.test.screen.ScreenQuadrinho;
import com.test.screen.Utils;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class LoginSteps {

    ScreenLogin login = new ScreenLogin();
    ScreenQuadrinho quadrinho = new ScreenQuadrinho();

    Utils utils = new Utils();

    @E("^preencho os campos e-mail e senha na tela de login$")
    public void preencho_os_campos_e_mail_e_senha_na_tela_de_login() {
        login.writeEmail();
        login.senhaLogin();
    }

    @Quando("^clicar no botão login$")
    public void clicar_no_botão_login() {
        login.btnLogin.click();
    }

    @E("^o aplicativo apresenta a tela de quadrinhos$")
    public void o_aplicativo_apresenta_a_tela_de_quadrinhos()  {
        quadrinho.tituloTelaQuadrinhos.isDisplayed();
    }

    @Dado("^acesso o menu perfil$")
    public void acesso_o_menu_perfil()  {
        login.botaoPerfil.click();
        login.tituloTelaPerfil.isDisplayed();
    }
    @E("^acesso o menu senha$")
    public void acesso_o_menu_senha()  {
        login.menuSenha.click();
    }

    @Quando("^altero o campo senha$")
    public void altero_o_campo_senha()  {
       login.escreverNovaSenha();
       login.repitaNovaSenha();
       login.botaoAlterar.click();
    }

    @Entao("^o aplicativo apresenta mensagem senha alterada com sucesso,$")
    public void o_aplicativo_apresenta_mensagem_senha_alterada_com_sucesso()  {
        login.msgSenhaAlteradaComSucesso.isDisplayed();
    }

    @Quando("^clico no botao Logout$")
    public void clico_no_botao_Logout()  {
       login.botaoLogout.click();

    }

    @Entao("^deslogo do aplicativo$")
    public void deslogo_do_aplicativo() {
        login.msgDesejaDeslogarDaConta.isDisplayed();
        login.clicarOpcaoSIM.click();
        login.tituloInicialDoApp.isDisplayed();
    }



}