package com.test.steps;

import com.test.Hooks;
import com.test.screen.*;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class CarrinhoCompraSteps {

    ScreenHome home = new ScreenHome();
    ScreenCarrinho cart = new ScreenCarrinho();
    Utils utils = new Utils();
    Hooks hooks = new Hooks();
    ScreenPagamento pagamento = new ScreenPagamento();
    ScreenCarrinho carrinho = new ScreenCarrinho();

    @Dado("^que esteja na tela de carrinho$")
    public void que_esteja_na_tela_de_carrinho()  {
        carrinho.valotTotalCarrinho.isDisplayed();

    }

    @E("^clicar no botao Fazer Pagamento$")
    public void clicar_no_botao_Fazer_Pagamento()  {
        pagamento.botaoFazerPagamento.click();
    }

    @Quando("^eu preencher as informações do Cartao$")
    public void eu_preencher_as_informações_do_Cartao()  {

        pagamento.tituloTelaPagamento.isDisplayed();
        pagamento.preencherNumeroDoCartao();
        pagamento.preencherNomeCartao();
        pagamento.preencherValidadeCartao();
        pagamento.preencherCodSegurancaCartao();
        pagamento.botaoPagarAgora.click();
    }

    @Entao("^do aplicativo apresenta mensagem de compra com sucesso$")
    public void do_aplicativo_apresenta_mensagem_de_compra_com_sucesso()  {
        pagamento.mensagemCompraFinalizada.isDisplayed();
        pagamento.mensagemCompraComSucesso.isDisplayed();
        pagamento.botaoOk.click();

    }

    @Quando("^eu preencher as informações do Cartao vencidos$")
    public void eu_preencher_as_informações_do_Cartao_vencidos()  {
        pagamento.tituloTelaPagamento.isDisplayed();
        pagamento.preencherNumeroDoCartao();
        pagamento.preencherNomeCartao();
        pagamento.preencherValidadeCartaoVencido();
        pagamento.preencherCodSegurancaCartao();
        pagamento.botaoPagarAgora.click();
    }

    @Entao("^do aplicativo apresenta mensagem de compra nao efetivada$")
    public void do_aplicativo_apresenta_mensagem_de_compra_nao_efetivada() {
        pagamento.mensagemCompraFinalizada.isDisplayed();
        pagamento.mensagemCompraComSucesso.isDisplayed();
        pagamento.botaoOk.click();
    }

}
