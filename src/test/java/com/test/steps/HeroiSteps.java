package com.test.steps;

import com.test.screen.ScreenHeroi;
import com.test.screen.ScreenLogin;
import com.test.screen.ScreenQuadrinho;
import com.test.screen.Utils;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class HeroiSteps {

    ScreenLogin login = new ScreenLogin();
    ScreenHeroi heroi = new ScreenHeroi();

    Utils utils = new Utils();

    @Dado("^que estou na tela de lista de heróis$")
    public void que_estou_na_tela_de_lista_de_heróis() {
        heroi.botaoHeroi.click();


    }

    @Entao("^devo ver uma lista de heróis com imagens e nomes$")
    public void devo_ver_uma_lista_de_heróis_com_imagens_e_nomes()  {
        heroi.tituloTelaHerois.isDisplayed();

    }

    @Entao("^cada item da lista deve ter uma imagem, nome do heroi$")
    public void cada_item_da_lista_deve_ter_uma_imagem_nome_do_heroi()  {
        heroi.tituloTelaHerois.isDisplayed();
    }
    @Quando("^digito Captain na barra de pesquisa$")
    public void digito_Captain_na_barra_de_pesquisa() throws Throwable {
        heroi.barraPesquisaHeroi.click();
        heroi.escreverPesquisaHeroi();
    }

    @Entao("^devo ver uma lista de heróis que contém \"([^\"]*)\" no nome$")
    public void devo_ver_uma_lista_de_heróis_que_contém_no_nome(String arg1) {
        heroi.barraPesquisaHeroi.click();
        heroi.escreverPesquisaHeroi();
    }

    @Entao("^devo ver uma lista de heróis que contém Captain no nome$")
    public void devo_ver_uma_lista_de_heróis_que_contém_Captain_no_nome()  {
        heroi.nomeHeroiPesquisado.isDisplayed();
    }

    @Quando("^digito Teste na barra de pesquisa$")
    public void digito_Teste_na_barra_de_pesquisa() {
        heroi.escreverPesquisaHeroiInexistente();
    }

    @Entao("^nao devo ver uma lista de heróis que contém Teste no nome$")
    public void nao_devo_ver_uma_lista_de_heróis_que_contém_Teste_no_nome() {
        heroi.tituloTelaHerois.isDisplayed();
    }

}