package com.test.screen;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.remote.RemoteWebElement;

public class ScreenPagamento extends BaseScreen {

    @AndroidFindBy(xpath = "//android.view.View[@text=\"Pagamento\"]")
    public RemoteWebElement tituloTelaPagamento;

    @AndroidFindBy(xpath = "//android.widget.Button[@text=\"Fazer Pagamento\"]")
    public RemoteWebElement botaoFazerPagamento;

    @AndroidFindBy(xpath = "//android.view.View[1]/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText")
    public MobileElement campoNumeroDoCartao;

    @AndroidFindBy(xpath = "//android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText")
    public MobileElement campoNomeImpresso;

    @AndroidFindBy(xpath = "//android.view.View[3]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText")
    public MobileElement campoValidade;

    @AndroidFindBy(xpath = "//android.view.View[4]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText\n")
    public MobileElement campoCodSeguranca;

    @AndroidFindBy(xpath = "//android.widget.Button[@text=\"Pagar agora!\"]")
    public RemoteWebElement botaoPagarAgora;

    @AndroidFindBy(xpath = "//android.view.View[@text= \"Compra finalizada!\"]")
    public RemoteWebElement mensagemCompraFinalizada;

    @AndroidFindBy(xpath = "//android.view.View[@text= \"Compra finalizada!\"]")
    public RemoteWebElement mensagemCompraComSucesso;

    @AndroidFindBy(xpath = "//android.widget.Button[@text= \"OK\"]")
    public RemoteWebElement botaoOk;


    public void preencherNumeroDoCartao() {
        campoNumeroDoCartao.sendKeys("1234 5678 9876 5432");
    }

    public void preencherNomeCartao() {
        campoNomeImpresso.sendKeys("John Teste");
    }

    public void preencherValidadeCartao() {
        campoValidade.sendKeys("032029");
    }
        public void preencherValidadeCartaoVencido() {
            campoValidade.sendKeys("012021");
        }

        public void preencherCodSegurancaCartao() {
            campoCodSeguranca.sendKeys("321");
        }

    }

