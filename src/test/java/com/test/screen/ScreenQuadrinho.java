package com.test.screen;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.remote.RemoteWebElement;

public class ScreenQuadrinho extends  BaseScreen{

    @AndroidFindBy(xpath = "//android.view.View[@text= \"Quadrinhos\"]")
    public RemoteWebElement tituloTelaQuadrinhos;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text= \"Fantastic Four by Dan Slott Vol. 1 (Hardcover)\"]")
    public RemoteWebElement listaQuadrinhos;

    @AndroidFindBy(xpath = "//android.widget.Image[@text= \"603d5b82a5bc0\"]")
    public RemoteWebElement capaQuadrinhos;

    @AndroidFindBy(xpath = "//android.view.View[@text= \"R$ 39.99\"]")
    public RemoteWebElement valorQuadrinho;

    @AndroidFindBy(xpath = "//android.view.View[2]/android.view.View/android.view.View[1]/android.view.View/android.widget.EditText")
    public MobileElement campoPesquisa;

    public void escreverPesquisa() {
        campoPesquisa.sendKeys("Hulkv");
    }

    @AndroidFindBy(xpath = "//android.widget.TextView[@text= \"Hulkverines (2019) #2\"]")
    public RemoteWebElement listaPesquisaQuadrinhos;

    @AndroidFindBy(xpath = "//android.widget.Image[@text= \"5c93f55981d71\"]")
    public RemoteWebElement capaPesquisaQuadrinhos;

    @AndroidFindBy(xpath = "//android.view.View[@text= \"R$ 4.99\"]")
    public RemoteWebElement valorPesquisaQuadrinho;

    @AndroidFindBy(xpath = "//android.view.View[@text= \"Descrição:\"]")
    public RemoteWebElement descricaoPesquisaQuadrinho;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text= \"R$ 4.99\"]")
    public RemoteWebElement valorPesquisaDescQuadrinho;

    @AndroidFindBy(xpath = "//android.widget.Button[@text= \"+\"]")
    public RemoteWebElement addQtdQuadrinho;

    @AndroidFindBy(xpath = "//android.widget.Button[@text= \"Adicionar R$ 4.99\"]")
    public RemoteWebElement addQuadrinhoAoCarrinho;








    }
