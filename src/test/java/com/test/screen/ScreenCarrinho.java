package com.test.screen;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.remote.RemoteWebElement;

public class ScreenCarrinho extends  BaseScreen {

    @AndroidFindBy(xpath = "//android.view.View[@text= \"Carrinho\"]")
    public RemoteWebElement tituloTelaCarrinho;

    @AndroidFindBy(xpath = "//android.view.View[@text= \"Total: R$ 4.99\"]")
    public RemoteWebElement valotTotalCarrinho;


}
