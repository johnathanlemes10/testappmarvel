package com.test.screen;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.remote.RemoteWebElement;

import java.sql.Driver;


public class ScreenCadastro extends BaseScreen{
        Driver driver;

        public ScreenCadastro(RemoteWebElement remoteWebElement){
            this.driver = driver;
        }
    @AndroidFindBy(xpath = "//android.view.View[@text= \"Crie uma conta\"]")
    public RemoteWebElement botaoCriarUmaConta;

    @AndroidFindBy(xpath = "//android.view.View[@text= \"Cadastro\"]")
    public RemoteWebElement tituloCadastro;

    @AndroidFindBy(xpath = "//android.view.View[@text= \"Cadastro realizado com sucesso!\"]")
    public RemoteWebElement msgCadastroComSucesso;

    @AndroidFindBy(xpath = "//android.view.View[@text= \"Insira os dados.\"]")
    public RemoteWebElement msgCamposObrigatoriosNaoPreenchidos;

    @AndroidFindBy(xpath = "//android.view.View[@text= \"Error: The email address is already in use by another account.\"]")
    public RemoteWebElement msgEmailExistente;

    @AndroidFindBy(xpath = "//android.view.View[@text= \"Error: The email address is badly formatted.\"]")
    public RemoteWebElement msgEmailInvalido;





}
