package com.test.screen;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.remote.RemoteWebElement;

public class ScreenHeroi extends  BaseScreen{


    @AndroidFindBy(xpath = "//android.view.View[@text= \"people Heróis\"]")
    public RemoteWebElement botaoHeroi;

    @AndroidFindBy(xpath = "//android.view.View[@text= \"Heróis\"]")
    public RemoteWebElement tituloTelaHerois;

    @AndroidFindBy(xpath = "(//android.view.View/android.view.View/android.widget.EditText)[1]")
    public RemoteWebElement barraPesquisaHeroi;

    public void escreverPesquisaHeroi() {
        barraPesquisaHeroi.sendKeys("captain");
    }
    public void escreverPesquisaHeroiInexistente() {
        barraPesquisaHeroi.sendKeys("teste");
    }
    @AndroidFindBy(xpath = "//android.widget.TextView[@text= \"Captain America\"]")
    public RemoteWebElement nomeHeroiPesquisado;









}

