package com.test.screen;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.remote.RemoteWebElement;

public class ScreenLogin extends BaseScreen {

    @AndroidFindBy(xpath = "//android.widget.EditText")
    public MobileElement fieldEmail;

    @AndroidFindBy(xpath = "//android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText")
    public MobileElement fieldPass;

    @AndroidFindBy(xpath = "//android.view.View[3]/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText")
    public MobileElement fieldPass1;

    @AndroidFindBy(xpath = "//android.view.View[5]/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText")
    public MobileElement campoLogin;

    @AndroidFindBy(xpath = "//android.widget.Button[@text= \"Login\"]")
    public MobileElement btnLogin;

    @AndroidFindBy(xpath = "//android.widget.Button[@text= \"Cadastrar\"]")
    public MobileElement btnCadastrar;

    public void writeEmail() {
        fieldEmail.sendKeys("tst1@teste.com");
    }

    public void writeEmailInvalido() {
        fieldEmail.sendKeys("tst1");
    }

    public void writePass() {
        fieldPass.sendKeys("123456");
    }

    public void senhaLogin() {
        campoLogin.sendKeys("123456");
    }

    public void writePass1() {
        fieldPass1.sendKeys("123456");
    }
    @AndroidFindBy(xpath = "//android.view.View[@text= \"person Perfil\"]")
    public RemoteWebElement botaoPerfil;

    @AndroidFindBy(xpath = "//android.view.View[@text= \"Senha\"]")
    public RemoteWebElement menuSenha;

    @AndroidFindBy(xpath = "//android.view.View/android.view.View[1]/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText")
    public RemoteWebElement campoNovaSenha;

    @AndroidFindBy(xpath = "//android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.widget.EditText")
    public RemoteWebElement campoRepitaNovaSenha;

    @AndroidFindBy(xpath = "//android.widget.Button[@text= \"Alterar\"]")
    public RemoteWebElement botaoAlterar;

    @AndroidFindBy(xpath = "//android.view.View[2]/android.view.View/android.view.View")
    public RemoteWebElement msgSenhaAlteradaComSucesso;

    public void escreverNovaSenha() {
        campoNovaSenha.sendKeys("123456");
    }

    public void repitaNovaSenha() {
        campoRepitaNovaSenha.sendKeys("123456");
    }

    @AndroidFindBy(xpath = "//android.view.View[@text= \"Mudar Senha\"]")
    public RemoteWebElement tituloTelaHome;

    @AndroidFindBy(xpath = "//android.view.View[@text= \"Perfil\"]")
    public RemoteWebElement tituloTelaPerfil;


    @AndroidFindBy(xpath = "//android.view.View[@text= \"Logout\"]")
    public RemoteWebElement botaoLogout;

    @AndroidFindBy(xpath = "//android.view.View[@text= \"Deseja deslogar da sua conta?\"]")
    public RemoteWebElement msgDesejaDeslogarDaConta;

    @AndroidFindBy(xpath = "//android.widget.Button[@text= \"SIM\"]")
    public RemoteWebElement clicarOpcaoSIM;

    @AndroidFindBy(xpath = "//android.view.View[@text= \"M\"]")
    public RemoteWebElement tituloInicialDoApp;










}