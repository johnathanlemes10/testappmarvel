#language: pt

@Cadastro
Funcionalidade: Cadastro de usuário no app Marvellopedia
  Como um usuário
  Eu quero me cadastrar no app Marvellopedia
  Para ter acesso ao conteúdo exclusivo do aplicativo

  @CadastroSucesso
  Cenário: Cadastro de usuário válido
    Dado que estou na tela inicial do aplicativo Marvellopedia
    E clico no botão Criar uma conta
    E o aplicativo redireciona para a tela de cadastro
    E preencho os campos e-mail e senha
    Quando clicar no botão cadastrar
    Entao o aplicativo apresenta mensagem de usuario cadastrado com sucesso

  @CadastroEmailExistente
  Cenário: Tentativa de cadastro com e-mail já cadastrado
    Dado que estou na tela inicial do aplicativo Marvellopedia
    E clico no botão Criar uma conta
    E o aplicativo redireciona para a tela de cadastro
    E preencho os campos e-mail e senha
    Quando clicar no botão cadastrar
    Então o aplicativo apresenta mensagem de erro informando que o endereço de e-mail já está em uso
    E devo permanecer na tela de cadastro

  @CamposObrigatoriosNaoPreenchidos
  Cenário: Tentativa de cadastro sem preencher todos os campos obrigatórios
    Dado que estou na tela inicial do aplicativo Marvellopedia
    E clico no botão Criar uma conta
    E o aplicativo redireciona para a tela de cadastro
    Quando clicar no botão cadastrar
    Então devo receber uma mensagem de erro informando que a senha é obrigatória
    E devo permanecer na tela de cadastro

  @CadastroEmailInvalido
  Cenário: Tentativa de cadastro com e-mail já cadastrado
    Dado que estou na tela inicial do aplicativo Marvellopedia
    E clico no botão Criar uma conta
    E o aplicativo redireciona para a tela de cadastro
    E preencho o campo email com email inválido
    Quando clicar no botão cadastrar
    Então o aplicativo apresenta mensagem de Email invalido
    E devo permanecer na tela de cadastro