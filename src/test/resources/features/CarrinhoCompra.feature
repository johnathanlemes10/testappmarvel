#language: pt

@pedido
Funcionalidade: Realizar compra dos quadrinhos
  Como um usuário registrado
  Eu quero comprar quadrinhos no aplicativo


  Contexto: Login com sucesso
    Dado que estou na tela inicial do aplicativo Marvellopedia
    E preencho os campos e-mail e senha na tela de login
    E clicar no botão login
    E que estou na tela de quadrinhos
    E insiro um nome de quadrinho na barra de pesquisa
    Quando seleciono um quadrinho na lista de quadrinhos
    Então a tela de detalhes do quadrinho é exibida com capa do quadrinho, título, descrição e valor
    E clico no botao adicionar

  @CompraComSucesso
  Cenário: Compra com sucesso

    Dado que esteja na tela de carrinho
    E clicar no botao Fazer Pagamento
    Quando eu preencher as informações do Cartao
    Então do aplicativo apresenta mensagem de compra com sucesso


#  O Aplicativo permitiu o pagamento com data do cartao vencida
  @CompraComFalha
  Cenário: Compra com data cartao vencida

    Dado que esteja na tela de carrinho
    E clicar no botao Fazer Pagamento
    Quando eu preencher as informações do Cartao vencidos
    Então do aplicativo apresenta mensagem de compra nao efetivada




